package com.orange.fastronomy.maker.burger.web;

import com.orange.fastronomy.maker.burger.domain.Burger;
import com.orange.fastronomy.maker.burger.domain.Purchase;
import com.orange.fastronomy.maker.burger.service.BurgerMakerService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.time.Instant;

@Path("/api/burger")
@Tag(name = "Burger maker API", description = "This API delivers burgers.")
@Produces(MediaType.APPLICATION_JSON)
public class BurgerMakerController {

    @Inject
    protected BurgerMakerService burgerMakerService;

    @Operation(description = "Deliver a burger and logs the purchase")
    @APIResponse(responseCode = "200", description = "Burger successfully delivered")
    @APIResponse(responseCode = "503", description = "No burger available")
    @DELETE()
    @Transactional
    @Path("/top")
    public Burger topBurger() {
        Burger burger = burgerMakerService.deliver()
                .orElseThrow(() -> new NoBurgerException("Can not deliver a burger"));
        Purchase purchase = new Purchase(Instant.now(), burger);
        purchase.persist();
        return burger;
    }

    @Operation(description = "Returns purchase logs, sorted by date (descending)")
    @GET()
    @Path("/purchases")
    public PaginatedResponse<Purchase> findPurchases(
            @Parameter(description = "pagination page number (zero-based)")
            @QueryParam("page")
            @DefaultValue("0")
                    int page,
            @Parameter(description = "pagination page size")
            @QueryParam("size")
            @DefaultValue("10")
                    int size
    ) {
        return new PaginatedResponse(Purchase.findAll().page(page, size));
    }

}
