describe('Burger maker API Tests', () => {
    it('A burger should be delivered as expected', () => {
        cy.request('DELETE', '/api/burger/top')
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body).to.have.property('name', 'burger')
                expect(response.body).to.have.property('ingredients').lengthOf(9)
                expect(response.body.ingredients[0]).to.have.property('name', 'bun')
                expect(response.body.ingredients[1]).to.have.property('name', 'ketchup')
                expect(response.body.ingredients[2]).to.have.property('name', 'mayo')
                expect(response.body.ingredients[3]).to.have.property('name', 'tomato')
                expect(response.body.ingredients[4]).to.have.property('name', 'salad')
                expect(response.body.ingredients[5]).to.have.property('name', 'bacon')
                expect(response.body.ingredients[6]).to.have.property('name', 'cheddar')
                expect(response.body.ingredients[7]).to.have.property('name', 'steak')
                expect(response.body.ingredients[8]).to.have.property('name', 'cheddar')
            })
    })

    it('List purchases should work', () => {
        cy.request('/api/burger/purchases')
            .should((response) => {
                expect(response.status).to.eq(200)
                expect(response.body).to.have.property('totalElements')
                expect(response.body).to.have.property('elements')
            })
    })
})
