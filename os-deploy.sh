#!/bin/bash

set -e

echo "[burger-maker] apply script"

if ! oc get deploymentconfig "$environment_name-db" > /dev/null 2>&1 ; then
  oc apply -f openshift.yml
fi