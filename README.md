# Maven on RedHat OpenShift project sample

This project sample shows the usage of _to be continuous_ templates:

* Maven
* [RedHat OpenShift](https://www.redhat.com/en/technologies/cloud-computing/openshift)
* Cypress
* Postman

The project exposes a basic JSON/Rest API, testable with Swagger UI.

It is a [Quarkus](https://quarkus.io/) application, and uses a MariaDB database.

## Maven template features

This project uses the following features from the Maven template:

* Overrides the default Maven docker image by declaring the `$MAVEN_IMAGE` variable in the `.gitlab-ci.yml` file,
* Defines the `$SONAR_HOST_URL` (enables SonarQube analysis),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `pom.xml`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable.

The Maven template also implements [unit tests report](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) and
[code coverage](https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-parsing) integration in GitLab.

## OpenShift template features

This project uses [RedHat OpenShift](https://www.redhat.com/en/technologies/cloud-computing/openshift) to host the Java application.

This project uses the following features from the OpenShift template:

* Defines mandatory `$OS_URL` and `$OS_TOKEN`  in the project variables,
* Enables review environments by declaring the `$OS_REVIEW_PROJECT` in the project variables,
* Enables staging environment by declaring the `$OS_STAGING_PROJECT` in the project variables,
* Enables production environment by declaring the `$OS_PROD_PROJECT` in the project variables.

The GitLab CI OpenShift template also implements [environments integration](https://gitlab.com/to-be-continuous/samples/maven-on-OpenShift/-/environments) in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related development branch is deleted).

Lastly, this project also implements the cleanup all review env job:

* can be triggered manually from `master` branch pipelines,
* scheduled every evening from `cleanup` branch.

### implementation details

In order to perform OpenShift deployments, this project implements:

* `os-pre-apply.sh` hook script: creates a MariaDB database if does not exist
* `openshift.yml` template: instantiates all required OpenShift objects
* `os-post-apply.sh` hook script: create a source-to-image binary build if does not exist and triggers a build from the executable jar
* `os-readiness-check.sh` hook script: waits and checks for the application to respond on `/health` endpoint right after deployment

All those scripts and descriptors make use of variables dynamically evaluated and exposed by the GitLab CI OpenShift template:

* `${environment_name}`: the application target name to use in this environment (ex: `maven-on-openshift-review-fix-bug-12`)
* `${environment_name_ssc}`: the application target name in [SCREAMING_SNAKE_CASE](https://en.wikipedia.org/wiki/Snake_case) format
  (ex: `MAVEN_ON_OPENSHIFT_REVIEW_FIX_BUG_12`)
* `${stage}`: the CI job stage (equals `$CI_JOB_STAGE`)
* `${hostname}`: the environment hostname, extracted from `$CI_ENVIRONMENT_URL` (declared as [`environment:url`](https://docs.gitlab.com/ee/ci/yaml/#environmenturl) in the `.gitlab-ci.yml` file)

## Acceptance test

This project also implements its own (basic) acceptance test in the `.gitlab-ci.yml` file, by declaring an `acceptance`
stage and acceptance test jobs.