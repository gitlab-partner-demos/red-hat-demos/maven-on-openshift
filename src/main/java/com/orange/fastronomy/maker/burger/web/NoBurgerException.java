package com.orange.fastronomy.maker.burger.web;

import javax.ws.rs.ServiceUnavailableException;

public class NoBurgerException extends ServiceUnavailableException { // NOSONAR

    public NoBurgerException(String message) {
        super(message);
    }

}
