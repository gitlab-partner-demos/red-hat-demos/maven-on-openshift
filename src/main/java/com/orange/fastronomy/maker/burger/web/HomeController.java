package com.orange.fastronomy.maker.burger.web;

import org.eclipse.microprofile.openapi.annotations.Operation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.net.URI;

/**
 * Redirects / to Swagger UI
 */
@Path("/")
public class HomeController {
    @GET
    @Operation(hidden = true)
    public Response redirectToSwaggerUi() {
        return Response.temporaryRedirect(URI.create("/swagger-ui/")).build();
    }
}
