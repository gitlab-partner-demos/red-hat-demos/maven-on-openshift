#!/bin/bash

set -e

echo "[burger-maker] cleanup environment - deleting application"

oc delete all,pvc,is,secret -l "app=${environment_name}"

echo "[burger-maker] cleanup environment - deleting database"

oc delete all,pvc,is,secret -l "app=${environment_name}-db"