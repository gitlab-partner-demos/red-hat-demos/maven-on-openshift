#!/bin/bash

set -e

echo "[burger-maker] post apply script: trigger a build"

# prepare build resources
mkdir -p target/openshift/deployments && cp target/burger-maker-on-openshift-1.0.0-SNAPSHOT-runner.jar target/openshift/deployments/

# trigger build (jar -> Docker): this will trigger a deployment
oc start-build "$environment_name" --from-dir=target/openshift --wait --follow
