package com.orange.fastronomy.maker.burger.service;

import com.orange.fastronomy.maker.burger.domain.Burger;
import com.orange.fastronomy.maker.burger.domain.Ingredient;

import javax.enterprise.context.ApplicationScoped;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class BurgerMakerService {

    private static final String BURGER_NAME = "burger";

    public Optional<Burger> deliver() {
        return Optional.of(new Burger(
                UUID.randomUUID().toString(),
                BURGER_NAME,
                Arrays.asList(
                    new Ingredient(UUID.randomUUID().toString(), "bun"),
                    new Ingredient(UUID.randomUUID().toString(), "ketchup"),
                    new Ingredient(UUID.randomUUID().toString(), "mayo"),
                    new Ingredient(UUID.randomUUID().toString(), "tomato"),
                    new Ingredient(UUID.randomUUID().toString(), "salad"),
                    new Ingredient(UUID.randomUUID().toString(), "bacon"),
                    new Ingredient(UUID.randomUUID().toString(), "cheddar"),
                    new Ingredient(UUID.randomUUID().toString(), "steak"),
                    new Ingredient(UUID.randomUUID().toString(), "cheddar")
                )
        ));
    }
}
